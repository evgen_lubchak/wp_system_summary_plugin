<?php
/*
Plugin Name: System Summary
Plugin URI: https://bitbucket.org/evgen_lubchak/wp_system_summary_plugin/src/master/
Description:  Show system summary in wp admin page
Version: 1.0.0
Author: Evgen Lubchak
Author URI: https://bitbucket.org/evgen_lubchak/wp_system_summary_plugin/src/master/
License: GPLv2 retarded or later
Text Domain: http://wp-system-summary-plugin.loc/
*/
/*
Copyright >>---(*_*)---> Evgen Lubchak  (email: evgencs4@gmail.com)
Vinnitsa, UA  >>---(*_*)--->  Ukraine
*/

use SysSummaryPlugin\Controllers\SiteInfo as SiteInfo;
use SysSummaryPlugin\Controllers\UpdateSiteInfo as UpdateSiteInfo;
use SysSummaryPlugin\Controllers\SystemSummary as SystemSummaryHome;
use SysSummaryPlugin\Controllers\PHPInfo as PHPInfo;
use SysSummaryPlugin\Migrations\ActivationMigration as ActivationMigration;
use SysSummaryPlugin\Migrations\UninstallMigration as UninstallMigration;
use SysSummaryPlugin\Seeds\ActivationSeed as ActivationSeed;

class SystemSummary {

    private $activationMigration;
    private $activationSeed;
    private $siteInfo;
    private $updateSiteInfo;
    private $systemSummaryHome;
    private $phpInfo;

    const UPDATE_SITE_INFO_SLUG = 'update-site-info';
    const SYSTEM_SETTINGS_SLUG = 'system-settings';
    const SITE_INFO_SLUG = 'site-info';
    const PHP_INFO_SLUG = 'php-info';

    /**
     * SystemSummary constructor.
     */
    public function __construct() {
        register_activation_hook(__FILE__, [$this, 'pluginActivation' ]);
        register_uninstall_hook(__FILE__, [__CLASS__, 'pluginUninstall']);
        add_action('admin_menu',[$this, 'addAdminMenuItem']);
        add_action('admin_post_update_site_info_form', [$this, 'updateSiteInfoFormResponse']);
        add_action('admin_post_update_input_form', [$this, 'updateInputFormResponse']);
        $this->activationMigration = new ActivationMigration();
        $this->activationSeed = new ActivationSeed();
        $this->siteInfo = new SiteInfo();
        $this->updateSiteInfo = new UpdateSiteInfo();
        $this->systemSummaryHome = new SystemSummaryHome();
        $this->phpInfo = new PHPInfo();
    }

    /**
     * Show system-summary home page
     */
    public function systemSummaryPage() {
        $this->systemSummaryHome->showPage();
    }

    /**
     * Show site info page
     */
    public function siteInfoPage() {
        $this->siteInfo->showPage();
    }

    /**
     * Show update site info page
     */
    public function updateSiteInfoPage() {
        $this->updateSiteInfo->showPage();
    }

    /**
     * Show php info page
     */
    public function phpInfoPage() {
        $this->phpInfo->showPage();
    }

    /**
     * Update site info data
     */
    public function updateSiteInfoFormResponse() {
        $this->updateSiteInfo->updateSiteInfo($_POST);
    }


    /**
     * Update input info data
     */
    public function updateInputFormResponse() {
        $this->updateSiteInfo->updateInputInfo($_POST);
    }

    /**
     * Plugin activation functionality
     */
    public function pluginActivation() {
        $this->activationMigration->runActivationMigrations();
        $this->activationSeed->runActivationSeed();
    }

    /**
     * Plugin uninstall functionality
     */
    public static function pluginUninstall() {
        $uninstallMigration = new UninstallMigration();
        $uninstallMigration->runUninstallMigration();
    }

    /**
     * Add new admin menu item
     */
    public function addAdminMenuItem() {
        add_menu_page(
            'System summary',
            'System summary',
            'manage_options',
            self::SYSTEM_SETTINGS_SLUG,
            [$this, 'systemSummaryPage']
        );
        add_submenu_page(
            'system-settings',
            'Site info',
            'Site info',
            'manage_options',
            self::SITE_INFO_SLUG,
            [$this, 'siteInfoPage']
        );
        add_submenu_page(
            'system-settings',
            'Update site info',
            'Update site info',
            'manage_options',
            self::UPDATE_SITE_INFO_SLUG,
            [$this, 'updateSiteInfoPage']
        );
        add_submenu_page(
            'system-settings',
            'PHP info',
            'PHP info',
            'manage_options',
            self::PHP_INFO_SLUG,
            [$this, 'phpInfoPage']
        );
    }
}

require_once plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . 'tools' . DIRECTORY_SEPARATOR . 'autoLoader.php';

new SystemSummary();
