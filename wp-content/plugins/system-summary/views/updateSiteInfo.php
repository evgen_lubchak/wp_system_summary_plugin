<?php echo $args['breadcrumbs']; ?>
<h2>Update Site Info</h2>
<?php
if(isset($_GET['update_site_info_form'])) {
    echo '<h4>Successfully updated!</h4>';
}
?>
<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
    <label for="blogname">Blog Name</label><br>
    <input type="text" name="blogname" id="blogname" value="<?php echo $args['siteInfo']['blogname'] ?>" required maxlength="150"><br>
    <label for="blogdescription">Blog Description</label><br>
    <textarea name="blogdescription" id="blogdescription" required maxlength="500"><?php echo $args['siteInfo']['blogdescription'] ?></textarea>
    <input type="hidden" name="action" value="update_site_info_form"><br>
    <hr>
    <input type="submit" value="Update Site Info">
</form>

<h2>Update Input Info</h2>
<?php
if(isset($_GET['update_input_form'])) {
    echo '<h4>Successfully updated!</h4>';
}
?>
<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
    <?php
        $i1 = $args['inputData'][0];
        $i2 = $args['inputData'][1];
        $i3 = $args['inputData'][2];
        $i4 = $args['inputData'][3];
        $i5 = $args['inputData'][4];
    ?>
    <label for="<?php echo $i1['name']; ?>"><?php echo $i1['name']; ?></label><br>
    <input type="input" name="<?php echo $i1['name']; ?>" id="<?php echo $i1['name']; ?>" value="<?php echo $i1['value']; ?>" maxlength="150"><br>

    <label for="<?php echo $i2['name']; ?>"><?php echo $i2['name']; ?></label><br>
    <textarea name="<?php echo $i2['name']; ?>" id="<?php echo $i2['name']; ?>" maxlength="150"><?php echo $i2['value']; ?></textarea><br>

    <label for="<?php echo $i3['name']; ?>"><?php echo $args['inputData'][2]['name']; ?></label><br>
    <input type="checkbox"
            <?php echo ($i3['value']) == 'ON' ? ' checked ' : ' ' ?>
           id="<?php echo $i3['name']; ?>" name="<?php echo $i3['name']; ?>" value="ON"><br>

    <label for="<?php echo $i4['name']; ?>"><?php echo $i4['name']; ?></label><br>
    <input type="password" name="<?php echo $i4['name']; ?>" id="<?php echo $i4['name']; ?>" value="<?php echo $i4['value']; ?>" maxlength="150"><br>

    <label for="<?php echo $i5['name']; ?>"><?php echo $i5['name']; ?> (between 0 and 50):</label><br>
    <input type="range" id="<?php echo $i5['name']; ?>" name="<?php echo $i5['name']; ?>" value="<?php echo $i5['value']; ?>" min="0" max="50"><br>

    <input type="hidden" name="action" value="update_input_form">
    <hr>
    <input type="submit" value="Update Inputs Data">
</form>

<h2>Input Data In Loop</h2>
<form action="" method="post">
    <?php foreach ($args['inputData'] as $key => $val) { ?>
        <label for="<?php echo $val['name']; ?>"><?php echo $val['name']; ?></label>
        <input type="input" name="<?php echo $val['name']; ?>" id="<?php echo $val['name']; ?>" value="<?php echo $val['value']; ?>"
               required maxlength="150" disabled><br>
    <?php } ?>
    <input type="hidden" name="action" value="update_input_form"><br><hr>
</form>


