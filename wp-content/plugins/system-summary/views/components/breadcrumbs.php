<?php
function getPathBuSlug(string $slug) {
    $str = 'admin.php?page=' . $slug;
    return admin_url($str);
}

array_unshift($breadcrumbs,[
    'pageName' => 'Plugin Home',
    'pageSlug' => \SystemSummary::SYSTEM_SETTINGS_SLUG
]);

foreach ($breadcrumbs as $key => $crumb) {
    $href = getPathBuSlug($crumb['pageSlug']);
    $name = $crumb['pageName'];
    if ( isset($breadcrumbs[$key+1]) ) {
        echo "<a href='$href'>$name</a> / ";
    } else {
        echo "<span>$name</span>";
    }
}
