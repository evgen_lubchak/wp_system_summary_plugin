<?php echo $args['breadcrumbs']; ?>
<h2>Site info</h2>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Option Name</th>
        <th>Option Value</th>
        <th>Autoload</th>
    </tr>
    <?php foreach ($args['siteInfo'] as $key => $val) { ?>
    <tr>
       <td><?php echo $val['option_id']; ?></td>
       <td><?php echo $val['option_name']; ?></td>
       <td><?php echo $val['option_value']; ?></td>
       <td><?php echo $val['autoload']; ?></td>
    </tr>
    <?php } ?>
</table>

<h2>Default Category</h2>
<p>
    <?php print_r($args['defaultCategory']['name']); ?>
</p>

<h2>Category list</h2>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Slug</th>
        <th>Term Group ID</th>
    </tr>
    <?php foreach ($args['categoryList'] as $key => $val) { ?>
        <tr>
            <td><?php echo $val['term_id']; ?></td>
            <td><?php echo $val['name']; ?></td>
            <td><?php echo $val['slug']; ?></td>
            <td><?php echo $val['term_group']; ?></td>
        </tr>
    <?php } ?>
</table>
