<?php

namespace SysSummaryPlugin\Seeds;

use SysSummaryPlugin\Models\BaseModel as BaseModel;

class ActivationSeed extends BaseModel {

    /**
     * Run plugin activation seed
     */
    public function runActivationSeed() {
        // TODO :: add normal seed system, not 1 file for all seeds
        // TODO :: add error handle functionality
        // TODO :: add better checking
        $sql = "SELECT id FROM $this->tableInputsName WHERE name IN ('input1', 'input2', 'input3', 'input4', 'input5')";
        $result = $this->db->get_results($sql, ARRAY_A);
        if(count($result) === 0) {
            $this->db->insert($this->tableInputsName, ['name' => 'input1', 'value' => '']);
            $this->db->insert($this->tableInputsName, ['name' => 'input2', 'value' => '']);
            $this->db->insert($this->tableInputsName, ['name' => 'input3', 'value' => '']);
            $this->db->insert($this->tableInputsName, ['name' => 'input4', 'value' => '']);
            $this->db->insert($this->tableInputsName, ['name' => 'input5', 'value' => '']);
        }
    }
}
