<?php

namespace SysSummaryPlugin\Models;

class SiteInfo extends BaseModel {

    /**
     * Get site info data
     *
     * @return array
     */
    public function getSiteInfo() : array {
        $sql = "SELECT option_id, option_name, option_value, autoload FROM $this->tableOptionsName WHERE option_name IN (
          'blogname', 
          'siteurl', 
          'home', 
          'blogdescription',
          'admin_email',
          'mailserver_url',
          'mailserver_login',
          'mailserver_pass',
          'mailserver_port',
          'default_category',
          'default_comment_status',
          'default_ping_status',
          'posts_per_page',
          'date_format',
          'time_format',
          'links_updated_date_format',
          'blog_charset',
          'template',
          'stylesheet',
          'default_role'
        )";
        return $this->db->get_results($sql, ARRAY_A);
    }

    /**
     * Update site info
     *
     * @param array $data
     */
    public function updateSiteInfo(array $data) {
        $blogName = $data['blogname'];
        $blogDescription = $data['blogdescription'];
        $sql = "UPDATE $this->tableOptionsName
                SET option_value = (CASE option_name 
                                      WHEN 'blogname' THEN '$blogName'
                                      WHEN 'blogdescription' THEN '$blogDescription'
                                    END)
                WHERE option_name IN('blogname','blogdescription')";
        $this->db->query($sql);
    }
}
