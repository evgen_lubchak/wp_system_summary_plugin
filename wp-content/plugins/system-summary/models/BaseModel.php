<?php

namespace SysSummaryPlugin\Models;

class BaseModel {

    public $db;
    public $tableOptionsName;
    public $tableTermsName;
    public $tableInputsName;

    /**
     * BaseModel constructor.
     */
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
        $this->tableOptionsName = $this->db->prefix . 'options';
        $this->tableTermsName = $this->db->prefix . 'terms';
        $this->tableInputsName = $this->db->prefix . 'inputs';
    }
}