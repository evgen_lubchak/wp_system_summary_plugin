<?php

namespace SysSummaryPlugin\Models;

class Category extends BaseModel {

    /**
     * Get default category
     *
     * @return array
     */
    public function getDefaultCategory() : array {
        $sql = "SELECT term_id, name, slug, term_group FROM $this->tableTermsName
                WHERE term_id = (SELECT option_value FROM $this->tableOptionsName WHERE option_name = 'default_category' LIMIT 1) LIMIT 1";
        return $this->db->get_results($sql, ARRAY_A)[0];
    }

    /**
     * Get category list
     *
     * @return array
     */
    public function getCategoryList() : array {
        $sql = "SELECT term_id, name, slug, term_group FROM $this->tableTermsName";
        return $this->db->get_results($sql, ARRAY_A);
    }
}