<?php

namespace SysSummaryPlugin\Models;

class Inputs extends BaseModel {

    /**
     * Get inputs data
     *
     * @return array
     */
    public function getInputsData() : array {
        $sql = "SELECT id, name, value FROM $this->tableInputsName WHERE name IN (
          'input1', 
          'input2', 
          'input3',
          'input4',
          'input5'
        )";
        return $this->db->get_results($sql, ARRAY_A);
    }

    /**
     * Update inputs info
     *
     * @param array $data
     */
    public function updateInputInfo(array $data) {
        $input1 = $data['input1'];
        $input2 = $data['input2'];
        $input3 = $data['input3'];
        $input4 = $data['input4'];
        $input5 = $data['input5'];
        $sql = "UPDATE $this->tableInputsName
                SET value = (CASE name 
                                      WHEN 'input1' THEN '$input1'
                                      WHEN 'input2' THEN '$input2'
                                      WHEN 'input3' THEN '$input3'
                                      WHEN 'input4' THEN '$input4'
                                      WHEN 'input5' THEN '$input5'
                                    END)
                WHERE name IN('input1','input2', 'input3', 'input4', 'input5')";
        $this->db->query($sql);
    }
}