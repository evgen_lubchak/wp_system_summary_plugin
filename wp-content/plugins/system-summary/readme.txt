=== System Summary Plugin ===

When writing this module, it was decided not to use file names like *class.akismet.php* using a dot, because this makes
some confusion and difficulties in *filename + namespaces + autoload*  systems.

== Description ==

Adds 4 pages to the admin panel.
1 Home plugin page.
2 Information about the site.
3 Page for site name update.
4 PHP info page.

== Installation ==

Upload the System Summary plugin to your blog, activate it, and then use it.
