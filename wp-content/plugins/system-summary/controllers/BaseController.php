<?php

namespace SysSummaryPlugin\Controllers;

class BaseController {

    const VIEWS_DIR = ABSPATH . 'wp-content/plugins/system-summary/views/';
    const BREADCRUMBS_VIEW_FILE = ABSPATH . 'wp-content/plugins/system-summary/views/components/breadcrumbs.php';

    /**
     * Do redirect in admin pages
     *
     * @param string $slug
     * @param array $params
     */
    public function redirect(string $slug, array $params) {
        $paramStr = (count($params) === 1) ?'&' . $params[0] : implode("&", $params);
        $redirectStr = "admin.php?page=$slug" . $paramStr;
        wp_redirect(admin_url($redirectStr));
    }


    /**
     * Get breadcrumbs HTML string
     *
     * @param array $breadcrumbs
     * @return string
     */
    public function getBreadcrumbsHtml(array $breadcrumbs) : string {
        $path = self::BREADCRUMBS_VIEW_FILE;
        ob_start();
        include($path);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Render files in views dir
     *
     * @param $file
     * @param array $args
     */
    public function renderPage($file, array $args){
        $path = self::VIEWS_DIR . $file . '.php';
        ob_start();
        include($path);
        $content = ob_get_contents();
        ob_end_clean();
        echo $content;
    }
}