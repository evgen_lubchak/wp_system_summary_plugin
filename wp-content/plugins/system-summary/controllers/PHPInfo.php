<?php

namespace SysSummaryPlugin\Controllers;

class PHPInfo extends BaseController {

    /**
     * Render php info page
     */
    public function showPage() {
        $breadcrumbsHtml = $this->getBreadcrumbsHtml([[
            'pageName' => 'PHP Info',
            'pageSlug' => \SystemSummary::PHP_INFO_SLUG,
        ]]);
        $dataToRender = [
            'breadcrumbs' => $breadcrumbsHtml
        ];
        $this->renderPage('phpInfo', $dataToRender);
    }
}