<?php

namespace SysSummaryPlugin\Controllers;

use SysSummaryPlugin\Models\SiteInfo as SiteInfoModel;
use SysSummaryPlugin\Models\Inputs as InputsModel;

class UpdateSiteInfo extends BaseController {

    private $siteInfoModel;
    private  $inputsModel;

    /**
     * UpdateSiteInfo constructor.
     */
    public function __construct() {
        $this->siteInfoModel = new SiteInfoModel();
        $this->inputsModel = new InputsModel();
    }

    /**
     * Render update site info page
     */
    public function showPage() {
        $breadcrumbsHtml = $this->getBreadcrumbsHtml([[
            'pageName' => 'Update Site Info',
            'pageSlug' => \SystemSummary::UPDATE_SITE_INFO_SLUG,
        ]]);
        $inputData = $this->inputsModel->getInputsData();
        $data = [
            'siteInfo' => [
                'blogname' => get_option('blogname'),
                'blogdescription' => get_option('blogdescription')
            ],
            'inputData' => $inputData,
            'breadcrumbs' => $breadcrumbsHtml
        ];
        $this->renderPage('updateSiteInfo', $data);
    }

    /**
     * Update site info data
     *
     * @param array $request
     */
    public function updateSiteInfo(array $request) {
        // TODO add server validation and error handle
        $this->siteInfoModel->updateSiteInfo($request);
        $this->redirect(\SystemSummary::UPDATE_SITE_INFO_SLUG, ['update_site_info_form=true']);
    }

    /**
     * Update input info data
     *
     * @param array $request
     */
    public function updateInputInfo(array $request) {
        // TODO add server validation and error handle
        $this->inputsModel->updateInputInfo($request);
        $this->redirect(\SystemSummary::UPDATE_SITE_INFO_SLUG, ['update_input_form=true']);
    }
}