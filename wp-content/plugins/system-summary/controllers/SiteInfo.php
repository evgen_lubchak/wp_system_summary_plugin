<?php

namespace SysSummaryPlugin\Controllers;
use SysSummaryPlugin\Models\SiteInfo as SiteInfoModel;
use SysSummaryPlugin\Models\Category as CategoryModel;

class SiteInfo extends BaseController {

    private $siteInfoModel;
    private $categoryModel;

    /**
     * SiteInfo constructor.
     */
    public function __construct() {
        $this->siteInfoModel = new SiteInfoModel();
        $this->categoryModel = new CategoryModel();
    }

    /**
     * Render site info page
     */
    public function showPage() {
        $breadcrumbsHtml = $this->getBreadcrumbsHtml([[
            'pageName' => 'Site Info',
            'pageSlug' => \SystemSummary::SITE_INFO_SLUG,
        ]]);
        $siteInfo = $this->siteInfoModel->getSiteInfo();
        $defaultCategory = $this->categoryModel->getDefaultCategory();
        $categoryList = $this->categoryModel->getCategoryList();
        $dataToRender = [
            'siteInfo' => $siteInfo,
            'defaultCategory' => $defaultCategory,
            'categoryList' => $categoryList,
            'breadcrumbs' => $breadcrumbsHtml
        ];
        $this->renderPage('siteInfo', $dataToRender);
    }
}