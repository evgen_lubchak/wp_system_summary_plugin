<?php

namespace SysSummaryPlugin\Controllers;

class SystemSummary extends BaseController {

    /**
     * Render system summary home page
     */
    public function showPage() {
        $breadcrumbsHtml = $this->getBreadcrumbsHtml([]);
        $dataToRender = [
            'breadcrumbs' => $breadcrumbsHtml
        ];
        $this->renderPage('systemSummary', $dataToRender);
    }
}