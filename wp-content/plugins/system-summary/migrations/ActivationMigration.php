<?php

namespace SysSummaryPlugin\Migrations;

class ActivationMigration extends BaseMigration {

    /**
     * Run plugin activation database migrations
     */
    public function runActivationMigrations() {
        // TODO :: add normal migration system, not 1 file for all migration
        // TODO :: add error handle functionality
        $charset_collate = $this->db->get_charset_collate();
        $sql = "CREATE TABLE $this->tableInputsName (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            name varchar(155) NOT NULL,
            value tinytext NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
}