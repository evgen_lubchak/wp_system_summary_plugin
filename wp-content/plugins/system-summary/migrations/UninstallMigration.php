<?php

namespace SysSummaryPlugin\Migrations;

class UninstallMigration extends BaseMigration {

    /**
     * Run plugin uninstall database migration
     */
    public function runUninstallMigration() {
        $this->db->query("DROP TABLE IF EXISTS $this->tableInputsName");
    }
}