<?php

/**
 * Get file load path
 *
 * @param string $className
 * @param string $folderName
 * @param string $replaceString
 * @param string $fileExtension
 * @return string
 */
function getLoadPath(string $className, string $folderName, string $replaceString, string $fileExtension) : string {
    $classes_dir = realpath( ABSPATH . 'wp-content/plugins/system-summary' ) . DIRECTORY_SEPARATOR . $folderName . DIRECTORY_SEPARATOR;
    $class_file = str_replace($replaceString, '', $className) . $fileExtension;
    return $classes_dir . $class_file;
}

/**
 * Auto loader
 *
 * @param string $class_name
 */
function autoLoader(string $class_name) {
    if(false !== strpos($class_name, 'SysSummaryPlugin' )) {
        switch ($class_name) {
            case false !== strpos($class_name, 'Controllers' ):
                $path = getLoadPath($class_name, 'controllers', 'SysSummaryPlugin\Controllers\\', '.php');
                break;
            case false !== strpos($class_name, 'Models' ):
                $path = getLoadPath($class_name, 'models', 'SysSummaryPlugin\Models\\', '.php');
                break;
            case false !== strpos($class_name, 'Migrations' ):
                $path = getLoadPath($class_name, 'migrations', 'SysSummaryPlugin\Migrations\\', '.php');
                break;
            case false !== strpos($class_name, 'Seeds' ):
                $path = getLoadPath($class_name, 'seeds', 'SysSummaryPlugin\Seeds\\', '.php');
                break;
        }
        require_once $path;
    }
}

spl_autoload_register('autoLoader');
