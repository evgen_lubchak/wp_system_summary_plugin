<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2THv~ziMT/.fV<dgTK[W:F=gsrH}ZA_3^;[?XJ*^;AALWegzdxzpcIKGoKu2L/Oo' );
define( 'SECURE_AUTH_KEY',  'lt[l8DGt)KYyoC+3BZ./i(ybW|LD+Ap#!)jj?1EI$]+=&E)t$R/^tTg*#LY-{8np' );
define( 'LOGGED_IN_KEY',    'T!c:1Ah^9&:*VgoG6FSHB`[Xav:zTrga_&V!Kx?,j7*mp(_)5h@L[n~< nOOETWZ' );
define( 'NONCE_KEY',        '-kU.~t,74o]+`WpjERtL!^KRC?T3Q:$*.K?6eES]qRI9B>TzPXT*zq~-xTg09nO{' );
define( 'AUTH_SALT',        'A&y!30:r`0U~Id!V1m;N%o&W=}t>Sx}I<Qtxjq:OWKf.n?7vJH3 y1N*_}*mw[QJ' );
define( 'SECURE_AUTH_SALT', '&uTsFpI%FL;RL;wh1-&N*`k74%7tq-<o-A>Mf<* rii,pFJT}9E^aw#Ii`3Fy{7a' );
define( 'LOGGED_IN_SALT',   'ZFE<Zg=A&)Z?#66!y,]Z[IYsa6~&>=|Mkd&&15y!cznp!>}+rT$;C33.51*it{s3' );
define( 'NONCE_SALT',       '@nM3vXuq2IAydZMOj/@a1j`P+!9:Sz+WT$|k.z= Egun935 W4cR5}a;N:AV0fdV' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
